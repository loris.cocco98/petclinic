FROM maven:3.8.1 as BUILD
COPY . /app
WORKDIR app/
RUN ["mvn", "package", "-Dmaven.test.skip=true", "-P MySQL"]

FROM tomcat:8.5-jre11
COPY --from=BUILD /app/target/*.war /usr/local/tomcat/webapps/
EXPOSE 8080